package io.mend.dne.domain;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
class DneServiceImpl implements DneService {
    @Override
    public boolean isDneSequence(List<Integer> sequence) {
        if (sequence == null || sequence.size() < 3) {
            return false;
        }
        int ai = sequence.get(0), aj = sequence.get(0);
        boolean aiFound = false;
        for (int ak : sequence) {
            if (!aiFound && ai >= ak) {
                if (ai > ak) {
                    ai = ak;
                }
                aj = ak;
            } else {
                // ai < ak
                aiFound = true;
            }
            if (aiFound) {
                if (aj < ak) {
                    aj = ak;
                }
                if (ak < aj) {
                    return true;
                }
            }
        }
        return false;
    }

}
