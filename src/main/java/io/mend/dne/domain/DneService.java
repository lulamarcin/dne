package io.mend.dne.domain;

import java.util.List;

public interface DneService {

    /**
     * @param sequence - The sequence to check if it is a dne sequence
     * @return true if sequence is dne, false if not.
     */
    boolean isDneSequence(List<Integer> sequence);

}
