package io.mend.dne.api.rest;

import io.mend.dne.domain.DneService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/server")
@RequiredArgsConstructor
public class DneServiceRestApi {

    private final DneService dneService;

    @PostMapping
    public boolean isDneSequence(@RequestBody SequenceDto sequence) {
        return dneService.isDneSequence(sequence.seq());
    }

}
