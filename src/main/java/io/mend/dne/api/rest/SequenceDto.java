package io.mend.dne.api.rest;

import java.util.List;


record SequenceDto(
        List<Integer> seq
) {

}
