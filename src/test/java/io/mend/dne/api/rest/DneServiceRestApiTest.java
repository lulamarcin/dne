package io.mend.dne.api.rest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class DneServiceRestApiTest {

    private static final HttpHeaders headers = new HttpHeaders() {{
        set("content-type", "application/json");
    }};

    @Value(value = "${local.server.port}")
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void isDneSequence_whenRequestSampleInput1_thenOkResponse_andResultFalse() {
        //given
        HttpEntity<String> givenRequest = new HttpEntity<>("{\"seq\": [1,2,3,7]}", headers);
        //when
        Boolean actualResponse = restTemplate.postForObject(String.format("http://localhost:%d/server", port), givenRequest, Boolean.class);
        //then
        Assertions.assertFalse(actualResponse, "Sample input1 is not DNE");
    }

    @Test
    void isDneSequence_whenRequestSampleInput2_thenOkResponse_andResultTrue() {
        //given
        HttpEntity<String> givenRequest = new HttpEntity<>("{\"seq\": [1,3,2]}", headers);
        //when
        Boolean actualResponse = restTemplate.postForObject(String.format("http://localhost:%d/server", port), givenRequest, Boolean.class);
        //then
        Assertions.assertTrue(actualResponse, "Sample input2 is DNE");
    }

    @Test
    void isDneSequence_requestHasIncorrectValuesInSeq_thenBadRequestResponse() {
        //given
        HttpEntity<String> givenRequest = new HttpEntity<>("{\"seq\": [a,1,3,2}", headers);
        //when
        ResponseEntity<String> actualResponse = restTemplate.postForEntity(String.format("http://localhost:%d/server", port), givenRequest, String.class);
        //then
        Assertions.assertEquals(HttpStatusCode.valueOf(400), actualResponse.getStatusCode(), "Response should be bad request");
    }

}