package io.mend.dne.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import static java.util.Collections.emptyList;

class DneServiceImplTest {

    private final DneService dneService = new DneServiceImpl();

    private static List<Arguments> provideArgumentsForIsDneSequenceTest() {
        return List.of(
                Arguments.of("Null sequence is not DNE", null, false),
                Arguments.of("Empty sequence is not DNE", emptyList(), false),
                Arguments.of("When sequence.size() < 3 then is not DNE", List.of(1, 2), false),
                Arguments.of("DNE sequence may hav size() == 3", List.of(1, 3, 2), true),
                Arguments.of("DNE sequence may have size() == 3", List.of(1, 2, 3), false),
                Arguments.of("DNA sequence may have ai as the first element", List.of(5, 7, 6), true),
                Arguments.of("DNE sequence may have ak as the last item", List.of(5, 7, 10, 10, 10, 5), true),
                Arguments.of("DNE sequence may have first items grater than ai", List.of(5, 5, 5, 4, 1, 2, 3, 3, 2, 2), true),
                Arguments.of("DNE sequence may have last items grater than ak", List.of(5, 4, 1, 2, 3, 3, 2, 2, 10), true),
                Arguments.of("DNE sequence may consist of maximum int values", List.of(MAX_VALUE, MIN_VALUE, MAX_VALUE, MIN_VALUE), true),
                Arguments.of("Sample input 1 is not DNE", List.of(1, 2, 3, 7), false),
                Arguments.of("Sample input 2 is DNE", List.of(4, 1, 7, 8, 7, 2), true)
        );
    }

    @DisplayName("isDneSequenceTest")
    @ParameterizedTest(name = "{0}")
    @MethodSource("provideArgumentsForIsDneSequenceTest")
    void isDneSequenceTest(String message, List<Integer> givenSequence, boolean expectedResult) {
        //when
        boolean actualResult = dneService.isDneSequence(givenSequence);
        //then
        Assertions.assertEquals(expectedResult, actualResult, message);
    }

}