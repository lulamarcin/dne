# Dne application

## Hwo to build jar

./mvnw clean package

## How to run

java -jar target/dne-0.0.1-SNAPSHOT.jar

# Definition

For a sequence of n integers a1, a2, ..., an, a DNE sequence is a subsequence ai, aj, ak, such that i < j < k and ai <
ak < aj.

###### Sample Input 1:

```
[1, 2, 3, 7]
```

###### Sample Output 1:

```
false
// Explanation: There is no DNE sequence in the sequence.
```

###### Sample Input 2:

```
[4, 1, 7, 8, 7, 2]
```

###### Sample Output 2:

```
true
// Explanation: There is a DNE sequence in the sequence [1, 7, 2] or [1, 8 ,2] or [1, 8, 7].
```

## The server

Java server that gets a sequence of n integers as an input in JSON format
and checks whether there is a DNE sequence in the list.

```
$ curl -X POST http://localhost:10000/server -H "content-type:application/json" -d '{"seq":[1,2,3]}'
// Result: false
```

```
$ curl -X POST http://localhost:10000/server -H "content-type:application/json" -d '{"seq":[1,3,2]}'
// Result: true
```

